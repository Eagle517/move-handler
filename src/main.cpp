#include <stdio.h>
#include <windows.h>

#include "TSFuncs.hpp"

const int gMaxTriggerKeys = 5; //This seems to be 5 in Blockland rather than 6 as shown in TGE source

struct Move
{
	int px, py, pz;
	unsigned int pyaw, ppitch, proll;
	float x, y, z;
	float yaw, pitch, roll;
	unsigned int id;
	unsigned int sendCount;

	bool freeLook;
	bool trigger[gMaxTriggerKeys];
};

void moveToString(const Move *const move, char *const buffer, size_t size)
{
	if(!move)
	{
		buffer[0] = '0';
		buffer[1] = '\0';
	}
	else
	{
		unsigned int flags = move->freeLook ? 1 : 0;
		for(int i = 0; i < gMaxTriggerKeys; ++i)
		{
			if(move->trigger[i])
				flags |= 1 << (i + 1);
		}

		snprintf(buffer, size, "%f %f %f %f %f %f %u",
		    move->x, move->y, move->z,
		    move->yaw, move->pitch, move->roll,
		    flags);
	}
}

void stringToMove(Move *const move, const char *const buffer)
{
	unsigned int flags = 0;

	sscanf(buffer, "%f %f %f %f %f %f %u",
	    &move->x, &move->y, &move->z,
	    &move->yaw, &move->pitch, &move->roll,
	    &flags);

	move->freeLook = (flags & 1) != 0;
	for (int i = 0; i < gMaxTriggerKeys; ++i)
		move->trigger[i] = (flags & (1 << (i + 1))) != 0;
}

BlFunctionDef(void, __thiscall, Player__processTick, ADDR *, const Move *);
void __fastcall Player__processTickHook(ADDR *, void *, const Move *);
BlFunctionHookDef(Player__processTick);

void __fastcall Player__processTickHook(ADDR *objPtr, void *blank, const Move *move)
{
	ADDR obj = (ADDR)objPtr;
	if(!(*(ADDR *)(obj + 68) & 2))
	{
		char id[16];
		snprintf(id, 16, "%d", *(ADDR *)(obj + 32));

		char buf[512];
		moveToString(move, buf, 512);

		tsf_BlCon__executef(3, "onPlayerProcessTick", id, buf);
	}

	Player__processTickHookOff();
	Player__processTick(objPtr, move);
	Player__processTickHookOn();
}

BlFunctionDef(bool, __thiscall, AIPlayer__getAIMove, ADDR *, Move *);
bool __fastcall AIPlayer__getAIMoveHook(ADDR *, void *, Move *);
BlFunctionHookDef(AIPlayer__getAIMove);

bool __fastcall AIPlayer__getAIMoveHook(ADDR *objPtr, void *blank, Move *move)
{
	char id[16];
	snprintf(id, 16, "%d", *(ADDR *)((ADDR)objPtr + 32));

	const char *res = tsf_BlCon__executef(2, "onGetAIMove", id);

	bool ret;
	if(!res[0])
	{
		AIPlayer__getAIMoveHookOff();
		ret = AIPlayer__getAIMove(objPtr, move);
		AIPlayer__getAIMoveHookOn();
	}
	else if(res[0] == '0' && !res[1])
		ret = false;
	else
	{
		stringToMove(move, res);
		ret = true;
	}

	return ret;
}

bool init()
{
	BlInit;

	if(!tsf_InitInternal())
		return false;

	BlScanFunctionHex(Player__processTick, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 56 8B F1 57 8B 7D 08 89 74 24 18 89 7C 24 14");
	BlScanFunctionHex(AIPlayer__getAIMove, "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 56 89 4C 24 44");

	Player__processTickHookOn();
	AIPlayer__getAIMoveHookOn();

	BlPrintf("MoveHandler: init'd");

	return true;
}

bool deinit()
{
	Player__processTickHookOff();
	AIPlayer__getAIMoveHookOff();

	BlPrintf("MoveHandler: deinit'd");

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch(reason)
	{
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl MoveHandler(){}
