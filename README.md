# Move Handler
A DLL that lets you see the moves of players and control the moves of AIPlayers.

Credit to [Port](https://github.com/qoh) for the original version and [Gytyyhgfffff](https://github.com/bansheerubber) for the r2001 version.

## Usage
`onPlayerProcessTick(%player, %move)`

Called for every Player::processTick call in the engine. `%player` can be either a Player or an AIPlayer, but only a Player will have a valid `%move`. For AIPlayers or Players that are dead, `%move` will be `"0"`. Otherwise, a move is a string in the format of `"x y z yaw pitch roll flags"` in which `x` represents the relative left/right movement, `y` represents the relative forward/backward movement, `z` is usually `0`, `yaw` and `pitch` refer to left/right and up/down, `roll` is usually `0`, and `flags` is a value representing which actions a player is doing and what triggers a player has active. The values for the different flags are:
```
None                      = 0
FreeLook                  = 1
Trigger0 (primary fire)   = 2
Trigger1 (secondary fire) = 4
Trigger2 (jump)           = 8
Trigger3 (crouch)         = 16
Trigger4 (jet)            = 32
```
The value of `flags` can range from 0 to 63. If you want to check for a certain flag, it's best to use the `&` operator. For example, to check if a player is jetting, you can do `(flags & 32) != 0`. This expression will return `1` if the player is jetting, and `0` if they are not.

`onGetAIMove(%aiPlayer)`

Called for every AIPlayer::getAIMove call in the engine. Returning a move causes the AI to use that move. Returning `0` causes the AI to ignore any move. Returning an empty string causes the AI to use whatever move it would normally use. Do not simply do `return;` as it can cause unwanted behavior.
